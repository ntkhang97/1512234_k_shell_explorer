﻿// KExplorer.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "KExplorer.h"

#include <Windows.h>
#include <string>
#include <CommCtrl.h>
#include <shellapi.h>
#include <ShlObj.h>

//Dùng để sử dụng hàm StrCpy, StrNCat
#include <shlwapi.h>
#pragma comment(lib, "shlwapi.lib")

#define MAX_LOADSTRING 100
#pragma comment(lib, "comctl32.lib")
#pragma comment(linker,"\"/manifestdependency:type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")


#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name
HWND hWndTV, hWndLV, hWndSB;
RECT rClient;
HTREEITEM Root;
HIMAGELIST himl;
// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);
void				InitWindowElement(HWND);	// Khoi tao TreeView, List View, Status bar.
void				TreeView_LoadDesktop();
void				InitImageList();			//Khoi tao Image list cho Tree View va list view.
void				TreeView_LoadFolder(HTREEITEM hParent, IShellFolder *ParentFolder);
void				ListView_LoadFolder(IShellFolder *CurFolder);
void				TreeView_InsertNode(HWND hWnd, HTREEITEM hParent,
	WCHAR* buffer, int iImage, LPARAM f);		//Them mot node vao cay.
int					GetIndexImage(WCHAR* buffer);//Lay chi so cua icon trong image list
int					TreeView_GetIconIndex(WCHAR* buffer);//Lay chi so cua icon cho TreeView Item.
int					ListView_GetIconIndex(WCHAR* buffer, SHFILEINFO fi, DWORD dwFileAttributes);//Lay chi so cua icon cho ListView Item.
BOOL				isEmptyFolder(IShellFolder *F); //Kiem tra xem folder co child hay ko.

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_ LPWSTR    lpCmdLine,
	_In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	// TODO: Place code here.

	// Initialize global strings
	LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadStringW(hInstance, IDC_KEXPLORER, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance(hInstance, nCmdShow))
	{
		return FALSE;
	}

	HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_KEXPLORER));

	MSG msg;

	// Main message loop:
	while (GetMessage(&msg, nullptr, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int)msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEXW wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_KEXPLORER));
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = MAKEINTRESOURCEW(IDC_KEXPLORER);
	wcex.lpszClassName = szWindowClass;
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	hInst = hInstance; // Store instance handle in our global variable

	HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

	if (!hWnd)
	{
		return FALSE;
	}

	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	// Thao tac voi .ini file
	const int BUFFERSIZE = 260;
	WCHAR buffer[BUFFERSIZE];
	WCHAR curPath[BUFFERSIZE];
	WCHAR configPath[BUFFERSIZE];
	DWORD w, h;
	GetCurrentDirectory(BUFFERSIZE, curPath);
	wsprintf(configPath, L"%s\\config.ini", curPath);
	GetPrivateProfileString(L"app", L"width", L"0", buffer, BUFFERSIZE, configPath);
	w = wcstol(buffer, NULL, 10);

	GetPrivateProfileString(L"app", L"height", L"0", buffer, BUFFERSIZE, configPath);
	h = wcstol(buffer, NULL, 10);
	switch (message)
	{
		
	case WM_CREATE:
	{
		SetWindowPos(hWnd, 0, 0, 0, w, h, SWP_SHOWWINDOW);
		InitWindowElement(hWnd);
	}break;
	case WM_NOTIFY:
	{
		int wmId = LOWORD(wParam);
		LPNMTREEVIEW nmTreeView = (LPNMTREEVIEW)lParam;
		NMHDR hdrTV = nmTreeView->hdr;

		switch (wmId)
		{
		case ID_TREEVIEW:
		{
			//Xu ly su kien bam mo node.
			switch (hdrTV.code)
			{
			case TVN_ITEMEXPANDING:
			{
				//MessageBox(0, 0, 0, 0);
				HTREEITEM hPrev = nmTreeView->itemNew.hItem;
				TVITEMEX tv;
				tv.hItem = hPrev;
				TreeView_GetItem(hWndTV, &tv);
				IShellFolder *folder = (IShellFolder*)tv.lParam;

				if (hPrev == Root)
					TreeView_LoadFolder(NULL, folder);
				else
					TreeView_LoadFolder(hPrev, folder);

			}break;
			case TVN_SELCHANGED:
			{
				//MessageBox(0, 0, 0, 0);
				HTREEITEM hPrev = nmTreeView->itemNew.hItem;
				TVITEMEX tv;
				tv.hItem = hPrev;
				TreeView_GetItem(hWndTV, &tv);
				IShellFolder *folder = (IShellFolder*)tv.lParam;

				if (hPrev == Root)
					ListView_LoadFolder(folder);
				else
					ListView_LoadFolder(folder);

				break;
			}
			default:
				break;
			}		
		}break;
		case ID_LISTVIEW:
		{
			LPNMLISTVIEW nmListView = (LPNMLISTVIEW)(LPNMHDR)lParam;
			NMHDR hdrLV = nmListView->hdr;
			if (hdrLV.code == NM_DBLCLK)
			{
				LVITEM lv;
				lv.iItem = nmListView->iItem;
				ListView_GetItem(hWndLV, &lv);
				IShellFolder* folder = (IShellFolder*)lv.lParam;
				if (isEmptyFolder(folder) == 1)
					ListView_DeleteAllItems(hWndLV);
				else
					ListView_LoadFolder(folder);
				if (folder != NULL)
					folder->Release();
			}
			if (hdrLV.code == NM_CLICK)
			{
				WCHAR bufferSz[15];
				WCHAR bufferNme[50];
				LVITEM lv;
				lv.iItem = nmListView->iItem;
				ListView_GetItem(hWndLV, &lv);
				ListView_GetItemText(hWndLV, lv.iItem, 2, bufferSz, 10);
				ListView_GetItemText(hWndLV, lv.iItem, 0, bufferNme, 50);
				if (bufferSz != NULL)
					SendMessage(hWndSB, SB_SETTEXT, 1, (LPARAM)bufferSz);
				SendMessage(hWndSB, SB_SETTEXT, 0, (LPARAM)bufferNme);
			}
		}
		default:
			break;
		}
	}
	case WM_COMMAND:
	{
		int wmId = LOWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
	}
	break;
	case WM_SIZE:
	{
		DWORD W = LOWORD(lParam);
		DWORD H = HIWORD(lParam);
		WCHAR size[5];
		wsprintf(size, L"%d", W);
		WritePrivateProfileString(L"app", L"width", size, configPath);
		wsprintf(size, L"%d", H);
		WritePrivateProfileString(L"app", L"height", size, configPath);

		SetWindowPos(hWndTV, 0, 0, 0, rClient.right / 3, H - 25, SWP_SHOWWINDOW);
		SetWindowPos(hWndLV, 0, rClient.right / 3, 0, W - rClient.right / 3, H - 25, SWP_SHOWWINDOW);
		int sz[3];
		sz[0] = W / 4;
		sz[1] = 2 * W / 4;
		sz[2] = -1;
		SendMessage(hWndSB, SB_GETPARTS, 3, (LPARAM)&sz);
		SendMessage(hWndSB, WM_SIZE, 0, 0);
	}
	break;
	case WM_PAINT:
	{
		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(hWnd, &ps);
		// TODO: Add any drawing code that uses hdc here...
		EndPaint(hWnd, &ps);
	}
	break;
	case WM_DESTROY:
		ListView_DeleteAllItems(hWndLV);
		TreeView_DeleteAllItems(hWndTV);
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}

void InitListView(HWND hWndLV)
{
	LVCOLUMN lvCol;
	lvCol.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;
	lvCol.fmt = LVCFMT_LEFT;

	lvCol.cx = 230;
	lvCol.pszText = L"Tên";
	ListView_InsertColumn(hWndLV, 0, &lvCol);

	lvCol.fmt = LVCFMT_LEFT | LVCF_WIDTH;
	lvCol.cx = 200;
	lvCol.pszText = _T("Loại");
	ListView_InsertColumn(hWndLV, 1, &lvCol);

	lvCol.fmt = LVCFMT_RIGHT;
	lvCol.cx = 175;
	lvCol.pszText = _T("Kích thước (Byte)");
	ListView_InsertColumn(hWndLV, 2, &lvCol);

	lvCol.cx = 200;
	lvCol.pszText = _T("Ngày sửa đổi");
	ListView_InsertColumn(hWndLV, 3, &lvCol);
}

void InitWindowElement(HWND hWnd)
{
	static BOOL Flag = FALSE;
	GetClientRect(hWnd, &rClient);//Lay kich thuoc vung cua so.

	InitCommonControls();

	hWndTV = CreateWindowEx(0, WC_TREEVIEW, TEXT("Tree View"),
		WS_CHILD | WS_VISIBLE | WS_BORDER |
		WS_VSCROLL | TVS_HASLINES | TVS_HASBUTTONS | TVS_LINESATROOT | TVS_SINGLEEXPAND,
		0, 0, rClient.right / 3, rClient.bottom - 25, hWnd, (HMENU)ID_TREEVIEW, hInst, 0);

	hWndLV = CreateWindow(WC_LISTVIEWW, TEXT("List View"), WS_CHILD | WS_VISIBLE | WS_BORDER | WS_VSCROLL |
		LVS_REPORT, rClient.right / 3, 0, 2 * rClient.right / 3, rClient.bottom - 25, hWnd, (HMENU)ID_LISTVIEW, hInst, 0);

	hWndSB = CreateWindow(STATUSCLASSNAME, NULL, WS_CHILD | WS_VISIBLE | SBARS_SIZEGRIP,
		0, 0, 0, 0, hWnd, NULL, hInst, NULL);

	int size[3];
	size[0] = rClient.right / 4;
	size[1] = 2 * rClient.right / 4;
	size[2] = -1;
	SendMessage(hWndSB, SB_SETPARTS, 3, (LPARAM)&size);
	SendMessage(hWndSB, SB_SETMINHEIGHT, 20, (LPARAM)0);

	if (Flag == FALSE)
	{
		InitImageList();
		Flag = TRUE;
	}

	InitListView(hWndLV);

	//Khoi tao node goc la desktop
	TVINSERTSTRUCT tvins;
	tvins.item.mask = TVIF_TEXT | TVIF_CHILDREN | TVIF_PARAM | TVIF_IMAGE | TVIF_SELECTEDIMAGE;
	tvins.hParent = NULL;
	tvins.hInsertAfter = TVI_ROOT;
	tvins.item.cChildren = 1;

	int index = GetIndexImage(L"Desktop");
	tvins.item.iImage = index;
	tvins.item.iSelectedImage = index;
	tvins.item.pszText = _T("Desktop");

	IShellFolder *desktop;
	SHGetDesktopFolder(&desktop);
	tvins.item.lParam = (LPARAM)desktop;
	Root = TreeView_InsertItem(hWndTV, &tvins);
}

void InitImageList()
{
	himl = ImageList_Create(28, 28, ILC_MASK | ILC_COLOR32, 10, 0);
	HICON icon;
	icon = LoadIcon(hInst, MAKEINTRESOURCE(IDI_ICON1));//Folder
	ImageList_AddIcon(himl, icon);

	icon = LoadIcon(hInst, MAKEINTRESOURCE(IDI_ICON2));//Laptop
	ImageList_AddIcon(himl, icon);

	icon = LoadIcon(hInst, MAKEINTRESOURCE(IDI_ICON3));//C drive
	ImageList_AddIcon(himl, icon);

	icon = LoadIcon(hInst, MAKEINTRESOURCE(IDI_ICON4));//Desktop
	ImageList_AddIcon(himl, icon);

	icon = LoadIcon(hInst, MAKEINTRESOURCE(IDI_ICON5));//HDD
	ImageList_AddIcon(himl, icon);

	icon = LoadIcon(hInst, MAKEINTRESOURCE(IDI_ICON6));//File
	ImageList_AddIcon(himl, icon);

	icon = LoadIcon(hInst, MAKEINTRESOURCE(IDI_ICON7));//Recyle bin
	ImageList_AddIcon(himl, icon);

	icon = LoadIcon(hInst, MAKEINTRESOURCE(IDI_ICON8));//Control Panel
	ImageList_AddIcon(himl, icon);

	TreeView_SetImageList(hWndTV, himl, TVSIL_NORMAL);
	ListView_SetImageList(hWndLV, himl, LVSIL_SMALL);
}

BOOL isEmptyFolder(IShellFolder *F)
{
	if (F == NULL)
		return 1;
	LPENUMIDLIST enumList;
	DWORD enumFlag = SHCONTF_FOLDERS;
	LPITEMIDLIST pidl = NULL;
	F->EnumObjects(NULL, enumFlag, &enumList);
	if (enumList == NULL)
		return 1;
	DWORD result = enumList->Next(1, &pidl, 0);
	return pidl == NULL;
}

void ConvertSize(WCHAR* rs, DWORD size)
{
	WCHAR* str[] = { L" byte", L" KB", L" MB", L" GB" };
	rs[0] = NULL;
	short count = 0;
	float f = size;
	while (f >= 1024)
	{
		f /= 1024;
		count++;
	}
	int tmp = f * 100;

	std::wstring r = std::to_wstring(tmp / 100);
	r = r + L"." + std::to_wstring(tmp % 100) + str[count];
	StrCat(rs, r.c_str());
}

void TreeView_LoadFolder(HTREEITEM hParent, IShellFolder *ParentFolder)
{
	//Kiem tra neu node da co con thi ko load lai lan nua.
	HTREEITEM CurrentNode;
	if (hParent == NULL)
		CurrentNode = TreeView_GetChild(hWndTV, Root);
	else
		CurrentNode = TreeView_GetChild(hWndTV, hParent);

	if (CurrentNode == NULL && isEmptyFolder(ParentFolder) == 0)
	{
		LPENUMIDLIST enumList;
		DWORD enumFlag = SHCONTF_FOLDERS;
		ParentFolder->EnumObjects(NULL, enumFlag, &enumList);
		LPITEMIDLIST Pidl;
		DWORD result = enumList->Next(1, &Pidl, 0);
		IMalloc *pMalloc;

		while (result != S_FALSE)
		{
			if (result != NOERROR)
				break;

			WCHAR buffer[1024];
			STRRET StrRet;

			StrRet.uType = STRRET_CSTR;
			ParentFolder->GetDisplayNameOf(Pidl, SHGDN_NORMAL, &StrRet);
			StrRetToBufW(&StrRet, Pidl, buffer, 1024);
			IShellFolder *f;
			ParentFolder->BindToObject(Pidl, 0, IID_IShellFolder, (void**)&f);

			TreeView_InsertNode(hWndTV, hParent, buffer, 0, (LPARAM)f);

			SHGetMalloc(&pMalloc);
			pMalloc->Free(Pidl);
			pMalloc->Release();
			result = enumList->Next(1, &Pidl, 0);
		}
		enumList->Release();
	}
}

void ListView_LoadFolder(IShellFolder *CurFolder)
{
	ListView_DeleteAllItems(hWndLV);

	LPENUMIDLIST enumList;
	DWORD enumFlag = SHCONTF_FOLDERS | SHCONTF_NONFOLDERS;
	CurFolder->EnumObjects(NULL, enumFlag, &enumList);
	LPITEMIDLIST Pidl;
	if (enumList == NULL)
		return;
	DWORD result = enumList->Next(1, &Pidl, 0);
	IMalloc *pMalloc;

	int index = 0;
	LVITEM lv;
	lv.mask = LVCF_TEXT | LVIF_PARAM | LVIF_IMAGE;
	SHFILEINFO fi;
	while (result != S_FALSE)
	{
		if (result != NOERROR)
			break;
		WCHAR buffer[1024];

		STRRET StrRet;
		StrRet.uType = STRRET_CSTR;
		CurFolder->GetDisplayNameOf(Pidl, SHGDN_NORMAL, &StrRet);
		StrRetToBufW(&StrRet, Pidl, buffer, 1024);
		IShellFolder *f;
		CurFolder->BindToObject(Pidl, 0, IID_IShellFolder, (void**)&f);
		SHGetFileInfo((LPCTSTR)Pidl, FILE_ATTRIBUTE_READONLY, &fi, sizeof(fi), SHGFI_PIDL | SHGFI_TYPENAME | SHGFI_ATTRIBUTES);
		WIN32_FIND_DATA findData;
		SHGetDataFromIDList(CurFolder, Pidl, SHGDFIL_FINDDATA, &findData, sizeof(WIN32_FIND_DATA));

		//Lay ten
		lv.iItem = index;
		lv.iSubItem = 0;
		int idc = ListView_GetIconIndex(buffer,fi,findData.dwFileAttributes);
		lv.iImage = idc;
		lv.lParam = (LPARAM)(f);
		ListView_InsertItem(hWndLV, &lv);
		ListView_SetItemText(hWndLV, index, 0, buffer);
		ListView_SetItemText(hWndLV, index, 1, fi.szTypeName);


		if (
			StrStr(fi.szTypeName, L"folder") == NULL &&
			StrStr(fi.szTypeName, L"Folder") == NULL &&
			findData.dwFileAttributes != 0)
		{
			WCHAR* bsize = new WCHAR[15];
			ConvertSize(bsize, findData.nFileSizeLow);
			ListView_SetItemText(hWndLV, index, 2, bsize);

		}
		////Lấy ngày.
		SYSTEMTIME stUTC, stLocal;
		FileTimeToSystemTime(&findData.ftLastWriteTime, &stUTC);
		SystemTimeToTzSpecificLocalTime(NULL, &stUTC, &stLocal);

		TCHAR *b = new TCHAR[50];
		wsprintf(b, _T("%02d/%02d/%04d %02d:%02d %s"),
			stLocal.wDay, stLocal.wMonth, stLocal.wYear,
			(stLocal.wHour > 12) ? (stLocal.wHour / 12) : (stLocal.wHour),
			stLocal.wMinute,
			(stLocal.wHour > 12) ? (_T("PM")) : (_T("AM")));
		ListView_SetItemText(hWndLV, index, 3, b);

		index++;
		SHGetMalloc(&pMalloc);
		pMalloc->Free(Pidl);
		pMalloc->Release();
		result = enumList->Next(1, &Pidl, 0);
	}
	enumList->Release();
}

void TreeView_InsertNode(HWND hWnd, HTREEITEM hParent, 
	WCHAR* buffer, int iImage, LPARAM f)
{
	TVINSERTSTRUCT tvins;
	tvins.item.mask = TVIF_TEXT | TVIF_CHILDREN | TVIF_PARAM | TVIF_IMAGE;

	if (hParent == NULL)
		tvins.hParent = Root;
	else tvins.hParent = hParent;

	int index = TreeView_GetIconIndex(buffer);

	tvins.hInsertAfter = TVI_LAST;
	tvins.item.cChildren = 1;
	tvins.item.iImage = index;
	tvins.item.iSelectedImage = index;
	tvins.item.pszText = buffer;

	tvins.item.lParam = f;
	TreeView_InsertItem(hWndTV, &tvins);
}

int GetIndexImage(WCHAR* buffer)
{
	if (StrStr(buffer,L"folder")!=NULL || StrStr(buffer, L"Folder") != NULL)
		return 0;

	if (!StrCmp(L"This PC", buffer))
		return 1;

	if (StrStr(buffer, L"Local Disk (C:)") != NULL)
		return 2;

	if (!StrCmp(L"Desktop", buffer))
		return 3;

	if (StrStr(buffer, L"Disk") != NULL)
		return 4;

	if (StrStr(buffer, L"Bin") != NULL)
		return 6;

	if (StrStr(buffer, L"Control") != NULL)
		return 7;

	return -1;
}

int TreeView_GetIconIndex(WCHAR* buffer)
{
	int index = GetIndexImage(buffer);

	if (index == -1)
		return 0;
	return index;
}

int ListView_GetIconIndex(WCHAR* buffer, SHFILEINFO fi , DWORD dwFileAttributes)
{
	int index = GetIndexImage(buffer);

	if (
		StrStr(fi.szTypeName, L"folder") == NULL &&
		StrStr(fi.szTypeName, L"Folder") == NULL &&
		dwFileAttributes != 0)
		return 5;

	if (index == -1)
		return 0;

	return index;
}