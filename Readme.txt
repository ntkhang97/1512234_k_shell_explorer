﻿Họ và tên: Nguyễn Thịnh Khang
MSSV: 1512234
Email: thinhkhang97@gmail.com

Các yêu cầu đã làm được: (Các thao tác với lấy file hoặc folder đã được làm bằng shell)

TREEVIEW:
* Tạo ra TreeView bên trái, ListView bên phải.
* Tạo node root là My Computer
* Lấy danh sách các ổ đĩa và hiển thị kèm theo các Icon.
* Bắt sự kiện Expanding và hiển thị các thư mục con.

LISTVIEW:
*Hiển thị toàn bộ thư mục và tập tin dưới một đường dẫn.
*Bắt sự kiện bấm đôi vào thư mục và hiển thị toàn bộ thư mục con và tập tin.
*Tạo ra ListView có 4 cột: Tên, Loại , Thời gian chỉnh sửa, Dung lượng. (Lấy được tất cả các thuộc tính nhưng không biết cách ghép Icon của 
đối tượng vào listview)
*Hiển thị Icon của thư mục, đối với tập tin vì chưa lấy được Icon nên để cùng Icon với thư mục.

Status Bar
* Đã tạo được status gồm 3 phần: Phần 1: Tên của thư mục hoặc File. Phần 2: Kích thước của file.

INI file
* Đã tạo, lưu và lấy được các thông số về kích thước của cửa sổ chính.

Các luồng sự kiện chính:
1. Chạy chương trình lên sẽ thấy node This PC trên TreeView bên trái. Bấm vào sẽ xổ xuống các ổ đĩa
và hiển thị chúng bên ListView (Có các Icon đi kèm).
2. Nhấp chuột vào một ổ đĩa bên TreeView sẽ xổ xuống các thư mục bên trong ổ đĩa đó, 
đồng thời hiện ra các thư mục bên trong sang bên ListView. Nếu ổ đĩa đó rỗng
hoặc là ổ nhận đĩa CD nhưng chưa có đĩa ... thì sẽ không có gì xảy ra.
3. Nhấp chuột vào một node đã expanded thì node đó sẽ thu gọn lại.
4. Nhấp đôi vào một node bên ListView sẽ hiện ra toàn bộ thư mục con và tập tin bên trong. Nếu
node đó là tập tin thì sẽ xóa toàn bộ màn hình bên ListView.
5. Nhấp đôi vào file sẽ hiện ra cửa số trắng.
6. Nhấp chuột vào một thư mục hoặc file sẽ hiện ra tên và kích thước (nếu là file) của thư mục hoặc tập tin ở status bar.

Các vấn đề gặp phải:
1. Khi bấm vào một Icon của TreeView thì Icon chuyển thành Icon của Folder, sau đó bấm vào node khác thì icon cũ đè lên node vừa nãy.
2. Đôi lúc chạy còn lỗi nhưng ko tìm ra nguyên nhân (1/10 chạy)
3. Chưa làm điều chỉnh khung của TreeView và ListView.
Chương trình được build trên X86

Các nguồn tham khảo.
https://msdn.microsoft.com/en-us/library/windows/desktop/bb775075(v=vs.85).aspx
http://bcbjournal.org/articles/vol4/0002/Getting_shell_item_information.htm
https://msdn.microsoft.com/en-us/library/windows/desktop/bb759792(v=vs.85).aspx

Link Youtube: https://youtu.be/lBn_w5XsscU
Link BitBucket: https://bitbucket.org/ntkhang97/1512234_k_shell_explorer